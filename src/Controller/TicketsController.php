<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class TicketsController extends AbstractController
{

     /**
     * @Route("/", name="tickets_list")
     */
    public function tickets()
    {
        $tickets = $this->getDoctrine()->getRepository(Ticket::class)->findAll();

        return $this->render('tickets\tickets.html.twig',[
            'title' => 'Balticon Tickets - Zgłoszenia',
            'tickets'=> $tickets

        ]);
    }

    /**
     * @Route("tickets/user_tickets", name="user_tickets")
     */
    public function user_tickets()
    {

        return $this->render('tickets\user_tickets.html.twig',[
            'title' => 'Moje Zgłoszenia',
        ]);
    }

    /**
     * @Route("tickets/stats", name="stats")
     */
    public function tickets_stats()
    {

        return $this->render('tickets\stats.html.twig',[
            'title' => 'Statystyki',
        ]);
    }

    /**
     * @Route("/tickets/save")
     */
    public function save() {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $ticket = new Ticket();
        $ticket->setSubject('Nowe');
        $ticket->setDescription('Nie dziala sap');
        $ticket->setDate(new \DateTime());
        $ticket->setStatus('Otwarte');
        $user->addTicket($ticket);
        $em->persist($ticket);
        $em->flush();

        return new Response('Zgłoszenie zostało utworzone, id:'.$ticket->getId());
    }

    /**
    * @Route("/tickets/{id}", name="ticket_show")
    */
    public function show($id) {
    $ticket = $this->getDoctrine()
        ->getRepository(Ticket::class)
        ->find($id);
    $user = $ticket->getUser();
    

    if (!$ticket) {
        throw $this->createNotFoundException(
            'Nie znaleziono zgłoszenia id: '.$id
        );
        }

     return $this->render('tickets\details.html.twig', [
         'ticket' => $ticket,
         'user' => ($user !== null) ? $user->getImie() : '',
         'title' => 'Szczegóły zgłoszenia:'.$id,]);
    }
}

