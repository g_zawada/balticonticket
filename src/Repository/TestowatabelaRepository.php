<?php

namespace App\Repository;

use App\Entity\Testowatabela;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Testowatabela|null find($id, $lockMode = null, $lockVersion = null)
 * @method Testowatabela|null findOneBy(array $criteria, array $orderBy = null)
 * @method Testowatabela[]    findAll()
 * @method Testowatabela[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestowatabelaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Testowatabela::class);
    }

    // /**
    //  * @return Testowatabela[] Returns an array of Testowatabela objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Testowatabela
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
